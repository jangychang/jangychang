terraform {
  backend "s3" {
    bucket = "build-landing-aws-jenkins-terraform-jangychang"
    key    = "node-aws-jenkins-terraform.tfstate"
    region = "ap-northeast-2"
  }
}

